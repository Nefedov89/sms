#!/usr/bin/env bash

# Run migrations
function migrate()
{
    echo "Running migrations ..."
    php artisan migrate
    echo "-> Success"
}

# Run bower.
function run_bower()
{
    echo "Installing bower components ..."
    if [ ! `which bower` ]; then
      echo ""
      echo "--- Bower required ---"
      echo "Please install bower using the following command:"
      echo "sudo npm install -g bower && bower install --allow-root"
      echo "----------------------"
      echo ""
    else
      bower install --allow-root
      echo "-> Success"
    fi;
}

# Run gulp.
function run_gulp()
{
    echo "Running gulp tasks ..."
    if [ ! `which gulp` ]; then
      echo ""
      echo "--- Gulp required ---"
      echo "Please install Gulp using the following command:"
      echo "sudo npm install -g gulp && gulp"
      echo "---------------------"
      echo ""
    else
      gulp
      echo "-> Success"
    fi;
}

# Clear cache
function clear_cache()
{
    echo "Refreshing localization cache..."
    php artisan js-localization:refresh
    echo "-> Success"
}

# Running
migrate && run_bower && run_gulp && clear_cache;