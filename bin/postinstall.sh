#!/usr/bin/env bash

# Install assets
function check_npm()
{
    echo "Checking npm availability..."
    if [ ! `which npm` ]; then
      echo ""
      echo "--- Npm Required ---"
      echo "Please install node.js using the following command:"
      echo "curl -sL https://deb.nodesource.com/setup | sudo bash - && sudo apt-get install nodejs"
      echo "--------------------"
      echo ""
    fi;  
}

# install elixir
function install_npm_deps()
{
    echo "Installing Elixir..."
    npm install
}

# Run bower.
function run_bower()
{
    echo "Installing bower components ..."
    if [ ! `which bower` ]; then
      echo ""
      echo "--- Bower required ---"
      echo "Please install bower using the following command:"
      echo "sudo npm install -g bower && bower install --allow-root"
      echo "----------------------"
      echo ""
    else
      bower install --allow-root
      echo "-> Success"
    fi;
}

# Run gulp.
function run_gulp()
{
    echo "Running gulp tasks ..."
    if [ ! `which gulp` ]; then
      echo ""
      echo "--- Gulp required ---"
      echo "Please install Gulp using the following command:"
      echo "sudo npm install -g gulp && gulp"
      echo "---------------------"
      echo ""
    else
      gulp
      echo "-> Success"
    fi;
}

# Clear cache
function clear_cache()
{
    echo "Configuring IDE helper..."
    php artisan clear-compiled
    echo "-> Success"
    echo "Refreshing localization cache..."
    php artisan js-localization:refresh
    echo "-> Success"
}

# Running
check_npm && install_npm_deps && run_bower && run_gulp && clear_cache;