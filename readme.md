## Laravel PHP Framework

This is the Laravel 5.1 Starter Kit with preloaded admin theme https://sleeping-owl.github.io/

## Installation

1. Clone this repository to your project folder
2. Execute 'composer install' command
3. Set DB credentials in your .env file
4. Execute 'php artisan migrate'
5. Set admin user 'php artisan admin:administrators --new' and set name and password
6. Remove remote repository 'git remote rm origin' and .git folder 'rm -rf .git'

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
