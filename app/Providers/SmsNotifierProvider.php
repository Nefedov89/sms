<?php

namespace App\Providers;

use App\Services\SmsNotifier;
use Illuminate\Support\ServiceProvider;

class SmsNotifierProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Register SMS Notifier service.
        $this->app->bind('SmsNotifier', function() {
            return new SmsNotifier();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'SmsNotifier'
        ];
    }
}
