<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\SmsNotifier;

/**
 * Class SmsController
 * @package App\Http\Controllers
 */
class SmsController extends Controller
{
    /**
     * @var
     */
    private $smsNotifier;

    /**
     * Constructor.
     * @param \App\Services\SmsNotifier $smsNotifier
     */
    public function __construct(SmsNotifier $smsNotifier)
    {
        $this->smsNotifier = $smsNotifier;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->smsNotifier->getSmsStatus('201611-1000010');
    }
}
