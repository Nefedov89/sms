<?php

/**
 * Admin User model.
 */

use SleepingOwl\Admin\Admin;
use SleepingOwl\Admin\Columns\Column;
use SleepingOwl\Admin\Models\Form\FormItem;

Admin::model(App\Models\User::class)
    ->title('Users')
    ->columns(function () {
        Column::userAvatarColumn('user-avatar', trans('admin.field_labels.user.avatar'))->sortable(false);
        Column::string('name', trans('admin.field_labels.user.name'));
        Column::string('email', trans('admin.field_labels.user.email'));
        Column::string('created_at', trans('admin.field_labels.common.created'));
        Column::string('updated_at', trans('admin.field_labels.common.updated'));
    })->form(function () {
        FormItem::text('name', trans('admin.field_labels.user.name'));
        FormItem::text('email', trans('admin.field_labels.user.email'));
        FormItem::password('password', trans('admin.field_labels.user.password'));
        FormItem::UserAvatarFormItem();
    });
