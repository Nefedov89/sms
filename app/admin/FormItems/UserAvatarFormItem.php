<?php

namespace App\Admin\FormItems;

use SleepingOwl\Admin\Admin;
use SleepingOwl\Admin\Models\Form\FormItem;
use SleepingOwl\Admin\Models\Form\Interfaces\FormItemInterface;

/**
 * Class UserAvatarFormItem
 */
class UserAvatarFormItem implements FormItemInterface
{
    protected $label;

    /**
     * @return string
     */
    /**
     * @return null|\SleepingOwl\Admin\Models\Form\FormItem\Image
     */
    public function render()
    {
        $adminFormBuilder = Admin::instance()->formBuilder;
        $instance = $adminFormBuilder->getModel();

        if ($instance->exists) {
            return $adminFormBuilder->imageGroup('avatar', trans('admin.field_labels.user.avatar'), $instance);
        } else {
            return null;
        }
    }

    /**
     * @param $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {

    }

    /**
     * @return array|null
     */
    public function getValidationRules()
    {

    }

    /**
     * @return mixed
     */
    public function getDefault()
    {

    }

    /**
     * @param array $data
     */
    public function updateRequestData(&$data)
    {

    }
}

