<?php

namespace App\Admin\Columns;

use SleepingOwl\Admin\Columns\Column\BaseColumn;

/**
 * Class UserAvatarColumn
 * @package App\Admin\Columns
 */
class UserAvatarColumn extends BaseColumn
{
    public function render($instance, $totalCount)
    {
        $avatarUrl = $instance->getAvatarUrl();
        $avatarImg = !is_null($avatarUrl) ? \Html::image($avatarUrl) : \Html::image(asset('images/default_avatar.png')) ;

        return '<td>'.$avatarImg.'</td>';
    }
}
