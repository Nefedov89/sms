<?php

/*
 * Describe you custom columns and form items here.
 *
 * There is some simple examples what you can use:
 *
 *		Column::register('customColumn', '\Foo\Bar\MyCustomColumn');
 *
 * 		FormItem::register('customElement', \Foo\Bar\MyCustomElement::class);
 *
 * 		FormItem::register('otherCustomElement', function (\Eloquent $model)
 * 		{
 *			AssetManager::addStyle(URL::asset('css/style-to-include-on-page-with-this-element.css'));
 *			AssetManager::addScript(URL::asset('js/script-to-include-on-page-with-this-element.js'));
 * 			if ($model->exists)
 * 			{
 * 				return 'My edit code.';
 * 			}
 * 			return 'My custom element code';
 * 		});
 */

use App\Admin\Columns\UserAvatarColumn;
use App\Admin\FormItems\UserAvatarFormItem;
use SleepingOwl\Admin\Columns\Column;
use SleepingOwl\Admin\Models\Form\FormItem;

/**
 * User avatar custom column.
 */
Column::register('userAvatarColumn', UserAvatarColumn::class);

/**
 * User avatar form item.
 */
FormItem::register('UserAvatarFormItem', UserAvatarFormItem::class);
