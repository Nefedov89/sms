<?php

namespace App\Services;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

/**
 * Class SmsNotifier
 * @package Services
 */
class SmsNotifier
{
    const RESPONSE_DETAILS_PARAM_DELIMITER = '|';

    /**
     * @var mixed
     */
    private $apiId;

    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * Constructor.
     */
    public function __construct()
    {
        // Set helper properties.
        // Api ID.
        $this->apiId = config('sms-notifier.api_id');

        // Guzzle client.
        $this->client = $client = new Client(
            [
                'base_uri' => config('sms-notifier.base_uri'),
            ]
        );
    }

    /**
     * Sent SMS.
     *
     * @param $to
     *  Phone number of recipient.
     * @param $text
     *  SMS text.
     *
     * @return \App\Services\SmsNotifier|\Symfony\Component\HttpFoundation\Response|static
     */
    public function send($to, $text)
    {
        // Guzzle response.
        $response = $this->client->request(
            'POST',
            '/sms/send',
            [
                'query' => [
                    'api_id' => $this->apiId,
                    'to'     => $to,
                    'text'   => $text,
                ],
            ]
        );


        // Response from sms.ru.
        $smsResponse = $this->getSmsResponse($response, 'sms_send');
        $smsResponseDecoded = json_decode($smsResponse->getContent());

        // Record new sms item to DB.
        $responseDetailsStr = $smsResponseDecoded->details;

        if ($responseDetailsStr) {
            $responseDetailsArr = explode(
                self::RESPONSE_DETAILS_PARAM_DELIMITER,
                $smsResponseDecoded->details
            );
            $sid = reset($responseDetailsArr);

            if ($sid && $smsResponseDecoded->code == 100) {
                DB::table('sms_notifications')->insert(
                    [
                        'sms_id' => $sid,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]
                );
            }
        }

        return $smsResponse;
    }

    /**
     * Get SMS status.
     *
     * @param int $sid
     *
     * @return \App\Services\SmsNotifier|\Symfony\Component\HttpFoundation\Response|static
     */
    public function getSmsStatus($sid)
    {
        // Guzzle response.
        $response = $this->client->request(
            'GET',
            '/sms/status',
            [
                'query' => [
                    'api_id' => $this->apiId,
                    'id'     => $sid,
                ],
            ]
        );

        // Response from sms.ru.
        return $this->getSmsResponse($response, 'sms_status');
    }

    /**
     * Get balance.
     *
     * @return \App\Services\SmsNotifier|\Symfony\Component\HttpFoundation\Response|static
     */
    public function getMyBalance()
    {
        // Guzzle response.
        $response = $this->client->request(
            'GET',
            '/my/balance',
            [
                'query' => [
                    'api_id' => $this->apiId,
                ],
            ]
        );

        // Response from sms.ru.
        return $this->getSmsResponse($response, 'my_balance');
    }

    /**
     * Get sms day limit.
     *
     * @return \App\Services\SmsNotifier|\Symfony\Component\HttpFoundation\Response|static
     */
    public function getMyLimit()
    {
        // Guzzle response.
        $response = $this->client->request(
            'GET',
            '/my/limit',
            [
                'query' => [
                    'api_id' => $this->apiId,
                ],
            ]
        );

        // Response from sms.ru.
        return $this->getSmsResponse($response, 'my_limit');
    }

    /**
     * Get send SMS count.
     *
     * @param null|integer $status
     *  SMS status.
     *
     * @return integer
     */
    public function getSmsCount($status = null)
    {
        $smsCount = DB::table('sms_notifications')->count();

        if (!is_null($status) && is_integer($status)) {
            $smsCount = DB::table('sms_notifications')
                ->where('sms_status', $status)
                ->count();
        }

        return $smsCount;
    }

    /**
     * Get response from sms.ru server.
     *
     * @param \GuzzleHttp\Psr7\Response $response
     * @param string                    $type
     *  Type of request ('sms_send', 'sms_status', 'my_limit', 'my_balance).
     *
     * @return \Symfony\Component\HttpFoundation\Response|static
     */
    private function getSmsResponse(GuzzleResponse $response, $type)
    {
        $responseContents = $response->getBody()->getContents();
        $responseData = ['response_text' => trans('sms-notifier.no_response')];

        if (!empty($responseContents)) {
            $smsResponse = explode(PHP_EOL, $responseContents);
            $smsResponseCode = array_shift($smsResponse);
            $smsResponseText = $smsResponseCode == 100
                ? trans('sms-notifier.response.100.'.$type)
                : trans('sms-notifier.response.'.$smsResponseCode);

            $responseData = [
                'code'    => $smsResponseCode,
                'text'    => $smsResponseText,
                'details' => implode(
                    self::RESPONSE_DETAILS_PARAM_DELIMITER,
                    $smsResponse
                ),
            ];
        }

        return JsonResponse::create($responseData, $response->getStatusCode());
    }
}