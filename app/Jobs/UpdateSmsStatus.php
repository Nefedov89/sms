<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Services\SmsNotifier;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Support\Facades\DB;

/**
 * Class UpdateSmsStatus
 * @package App\Jobs
 */
class UpdateSmsStatus extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $sid;

    /**
     * Create a new job instance.
     *
     * @param                           $sid
     */
    public function __construct($sid)
    {
        // Sms ID.
        $this->sid = $sid;
    }

    /**
     * Execute the job.
     *
     * @param \App\Services\SmsNotifier $smsNotifier SmsNotifier service.
     *
     * @return void
     */
    public function handle(SmsNotifier $smsNotifier)
    {
        $smsResponseDecoded = json_decode(
            $smsNotifier->getSmsStatus($this->sid)
                ->getContent()
        );

        // Update DB record with new status.
        DB::table('sms_notifications')
            ->where('sms_id', $this->sid)
            ->update(['sms_status' => $smsResponseDecoded->code]);
    }
}
