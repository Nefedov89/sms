<?php

namespace App\Console;

use App\Jobs\UpdateSmsStatus;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    use DispatchesJobs;

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
            ->hourly();

        // Schedule sms status update.
        $schedule->call(function () {
            // Get all sms.
            $sms = DB::table('sms_notifications')->get();

            if (count($sms) != 0) {
                foreach ($sms as $item) {
                    $this->dispatch(new UpdateSmsStatus($item->sms_id));
                }
            }
        })->daily();
    }
}
