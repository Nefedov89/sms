<?php

namespace App\Models;

use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Validator;
use SleepingOwl\Models\Interfaces\ValidationModelInterface;
use SleepingOwl\Admin\Exceptions\ValidationException;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Models\Traits\HasMediaManipulationsTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

/**
 * Class User
 * @package App\Models
 */
class User extends Model implements
    AuthenticatableContract,
    CanResetPasswordContract,
    ValidationModelInterface,
    HasMediaConversions,
    ModelWithImageFieldsInterface,
    HasRoleAndPermissionContract
{
    use Authenticatable,
        CanResetPassword,
        HasRoleAndPermission,
        HasMediaTrait,
        HasMediaManipulationsTrait,
        ModelWithImageOrFileFieldsTrait {
        ModelWithImageOrFileFieldsTrait::__call insteadof HasRoleAndPermission;
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'avatar'];

    /**
     * Date mutation.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @param       $data
     * @param array $rules
     * @throws \SleepingOwl\Admin\Exceptions\ValidationException
     * @return void
     */
    public function validate($data, $rules = [])
    {
        $validator = Validator::make($data, $this->getValidationRules());

        if ($validator->fails()) {
            throw new ValidationException($validator->errors());
        }
    }

    /**
     * Auto-bcrypt password attribute
     *
     * @param string $value
     */
    public function setPasswordAttribute($value)
    {
        // Update password only if we receive one.
        if (empty($value)) {
            return;
        }

        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * Return model validation rules.
     *
     * @return array
     */
    public function getValidationRules()
    {
        return
            [
                'name'   => 'required|min:3|max:32',
                'email'  => 'required|email|unique:users,email,'.$this->getAttribute('id').',id',
                'avatar' => 'sometimes|max:4096|image',
            ];
    }

    /**
     * Return model images fields.
     *
     * @return array
     */
    public function getImageFields()
    {
        return [
            'avatar' => '',
        ];
    }

    /**
     * Set model image.
     *
     * @param $field
     * @param $image
     */
    public function setImage($field, $image)
    {
        switch ($field) {
            case 'avatar':
                // Set avatar.
                try {
                    // Remove old existing avatar.
                    if (!is_null($this->getAvatarUrl())) {
                        $this->clearMediaCollection('avatar');
                    }

                    $fpath = $image->getRealPath();
                    /** @type Media $media */
                    $media = $this
                        ->addMedia($fpath)
                        ->preservingOriginal()
                        ->toMediaLibrary('avatar');

                    $media->file_name = $image->getClientOriginalName();

                    // Save manipulations if possible.
                    if (method_exists($this, 'saveManipulations')) {
                        $this->saveManipulations($media);
                    }
                } catch (\Exception $e) {
                }
                break;
        }
    }

    /**
     * Register media conversion.
     */
    public function registerMediaConversions()
    {
        foreach ($this->getMediaManipulations() as $manipulation) {
            $conversion = $this
                ->addMediaConversion($manipulation['conversion'])
                ->setManipulations($manipulation['parameters'])
                ->performOnCollections($manipulation['collection']);

            if (array_key_exists('queued',
                    $manipulation) && true === $manipulation['queued']
            ) {
                $conversion->queued();
            } else {
                $conversion->nonQueued();
            }
        }
    }

    /**
     * Get an array of media manipulations.
     *
     * @return array
     */
    public function getMediaManipulations()
    {
        return [
            [
                'conversion' => 'medium',
                'parameters' => [
                    'w'   => 200,
                    'h'   => 200,
                    'fm'  => 'png',
                    'fit' => 'crop',
                ],
                'collection' => 'avatar',
            ],
            [
                'conversion' => 'small',
                'parameters' => [
                    'w'   => 50,
                    'h'   => 50,
                    'fm'  => 'png',
                    'fit' => 'crop',
                ],
                'collection' => 'avatar',
            ],
        ];
    }

    /**
     * Get avatar URL.
     *
     * @param string $conversion
     *
     * @return null|string
     */
    public function getAvatarUrl($conversion = 'small')
    {
        $avatar = null;
        $mediaItems = $this->getMedia('avatar');

        if ($mediaItems->count() == 0) {
            return null;
        }

        return $mediaItems->first()->getUrl($conversion);
    }
}
