<?php

namespace App\Models\Traits;

use Spatie\MediaLibrary\Media;

/**
 * Class HasMediaManipulationsTrait
 *
 * @package App\Entities\Traits
 */
trait HasMediaManipulationsTrait
{
    /**
     * Save media specific manipulations into the model
     *
     * @param Media $media
     * @param bool  $autoSave
     */
    public function saveManipulations(Media $media, $autoSave = true)
    {
        if (method_exists($this, 'getMediaManipulations')) {
            $manipulations = [];

            foreach ($this->getMediaManipulations() as $manipulation) {
                $manipulations[$manipulation['conversion']] = $manipulation['parameters'];
            }
            $media->manipulations = $manipulations;

            if ($media->isDirty() && $autoSave) {
                $media->save();
            }
        }
    }
}
