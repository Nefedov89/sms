<?php

return [
    'js' => [

    ],
    'field_labels' => [
        'common' => [
            'created' => 'Created at',
            'updated' => 'Updated at',
        ],
        'user'   => [
            'avatar'   => 'Avatar',
            'name'     => 'Name',
            'email'    => 'Email',
            'password' => 'Password',
        ],
    ],
];
